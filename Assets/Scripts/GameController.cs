﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController gc;
    public Text levels;
    public int levelsBeat;

    public bool loadScene;

    public GameObject[] backgrounds;
    private GameObject background;
    public GameObject startPlatform;
    public GameObject finishPlatform;
    public GameObject mountains;

    void Awake()
    {
        levels.text = "Levels beat: 0";

        // Create a background to start each game 
        background = backgrounds[(int)Random.Range(0, backgrounds.Length)];

        Instantiate(background, background.transform.position, background.transform.rotation);

        // New platforms for every game 
        Vector2 startPosition = new Vector2(-7.5f, Random.Range(-2.4f, 3f));
        Instantiate(startPlatform, startPosition, startPlatform.transform.rotation);

        Vector2 finishPosition = new Vector2(7.5f, Random.Range(-4f, 3.5f));
        Instantiate(finishPlatform, finishPosition, finishPlatform.transform.rotation);

        // Height of mountains affected every game 
        Vector2 mountainsPosition = new Vector2(-1.09f, Random.Range(-3.5f, -1.97f));
        Instantiate(mountains, mountainsPosition, mountains.transform.rotation);

        // For restarting scene once hitting platform 
        if (gc == null)
        {
            gc = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            loadScene = false;
            Destroy(gameObject);
            levels.text = "Levels beat: " + levelsBeat;
            return;
        }
    }

    void Update()
    {
        levels.text = "Levels beat: " + levelsBeat;
    }
}
