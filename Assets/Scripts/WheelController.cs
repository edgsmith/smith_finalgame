﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelController : MonoBehaviour
{
    public PlayerController pc;

    // Check if wheels on the ground 
    void OnCollisionEnter2D()
    {
        pc.onGround = true;
    }

    void OnCollisionExit2D()
    {
        pc.onGround = false;
    }
}
