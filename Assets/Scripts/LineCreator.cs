﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineCreator : MonoBehaviour
{
    public GameObject linePrefab;
    LineController activeLine;

    void Update()
    {
        // Make line if mouse down 
        if (Input.GetMouseButtonDown(0))
        {
            GameObject newLine = Instantiate(linePrefab);
            activeLine = newLine.GetComponent<LineController>();
        }

        // No active line if up 
        if (Input.GetMouseButtonUp(0))
        {
            activeLine = null;
        }

        // Get mouse position 
        if (activeLine != null)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            activeLine.UpdateLine(mousePos);
        }
        
    }
}
