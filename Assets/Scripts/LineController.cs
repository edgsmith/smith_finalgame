﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LineController : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public EdgeCollider2D edgeCollider;

    // For the points of the line 
    List<Vector2> points;

    public void UpdateLine(Vector2 point)
    {
        // Insert starting point   
        if (points == null)
        {
            points = new List<Vector2>();
            SetPoint(point);
            return;
        }

        // Another point every time mouse moves 
        if (Vector2.Distance(points.Last(), point) > 0.1f)
        {
            SetPoint(point);
        }
    }

    void SetPoint(Vector2 point)
    {
        points.Add(point);
        lineRenderer.positionCount = points.Count;

        // Move to end 
        lineRenderer.SetPosition(points.Count - 1, point);

        // Enough points to make array 
        if (points.Count > 1)
        {
            edgeCollider.points = points.ToArray();
        }
    }
}
