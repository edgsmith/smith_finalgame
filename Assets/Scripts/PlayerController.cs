﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public float speed = 20f;
    public Rigidbody2D rb;

    public bool onGround = false;

    public float rotSpeed = 100f;
    Vector3 eulerRotation;

    AudioSource audios;
    public AudioClip[] engineNoises;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        eulerRotation = transform.rotation.eulerAngles;
        audios = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Move the car if button is down and the car is on the ground 
        if (Input.GetKey("up") && onGround)
        {
            rb.AddForce(transform.right * speed);
            audios.clip = engineNoises[Random.Range(0, engineNoises.Length)];
            audios.Play();
        }
        if (Input.GetKey("down") && onGround)
        {
            rb.AddForce(transform.right * -1f * speed);

            // Game makes acceleration noises 
            audios.clip = engineNoises[Random.Range(0, engineNoises.Length)];
            audios.Play();
        }

        // Rotating abilities 
        if (Input.GetKey("left"))
        {
            rb.AddTorque(Time.deltaTime * rotSpeed);
        }

        if (Input.GetKey("right"))
        {
            rb.AddTorque(Time.deltaTime * -rotSpeed);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        // If it hits platform restart scene 
        if (other.gameObject.CompareTag("finish"))
        {
            // Update the levels 
            GameController.gc.levelsBeat += 1;
            GameController.gc.loadScene = true;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        // Game over if hit a hazard 
        if (other.gameObject.CompareTag("hazard"))
        {
            GameController.gc.loadScene = true;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
